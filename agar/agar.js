function Cell(x, y, radius, col) {
	this.loc = createVector(x, y);
	this.velocity = createVector(0,0);
	this.col = color(red(col), green(col), blue(col));
	this.radius = radius;

	this.update = function() {

		/*var x = map(mouseX, 0, width, -2, 2);
		var y = map(mouseY, 0, height, -2, 2);
		var newvel = createVector(x, y);*/
		var newvel = createVector(mouseX-width/2, mouseY-height/2);
		newvel.setMag(2);
		this.velocity.lerp(newvel, 0.1);
		this.loc.add(this.velocity);
	}
	
	this.draw = function() {
		noStroke();
		fill(this.col);
		ellipse(this.loc.x, this.loc.y, this.radius*2, this.radius*2);
	}

	this.eats = function(blob) {
		var d = p5.Vector.dist(this.loc, blob.loc);
		if (d < this.radius + blob.radius) {
			//radius verhogen
			var area = PI*this.radius*this.radius + PI*blob.radius*blob.radius;
			/*
			Area = PI * R²
			R² = Area / PI
			R = sqrt(Area / PI)
			*/
			this.radius = sqrt(area/PI);
			return true;
		}
		return false;
	}

	this.keyPressed = function() {

	}	

	this.mouseMoved = function() {


	}
}
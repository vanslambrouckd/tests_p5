var cell;
var food = [];
var startRadius = 10;
var zoom = 1;

function setup() {	
	createCanvas(600, 400);
	frameRate(60);

	cell = new Cell(0,0, startRadius, color(255,255,255));
	for (var i = 0; i < 50; i++) {
		var x = random(-width, width);
		var y = random(-height, height);
		var radius = Math.floor(random(8,14));
		var col = color(Math.floor(random(255)),0,255);
		food[i] = new Cell(x, y, radius, col);
	}
}

function addNewFood(amount) {
	for (var i = 0; i < amount; i++) {
		var x = random(-width, width);
		var y = random(-height, height);
		var radius = Math.floor(random(8,14));
		var col = color(Math.floor(random(255)),0,255);
		food.push(new Cell(x, y, radius, col));
	}
}

function draw() {
	background(0);
	translate(width/2, height/2);
	//als je food opeet, zoomt de wereld een beetje uit
	var newzoom = sqrt(startRadius/cell.radius);
	zoom = lerp(zoom, newzoom, 0.1);
	scale(zoom);
	console.log(zoom);
	translate(-cell.loc.x, -cell.loc.y);
	//translate(-cell.loc.x, -cell.loc.y);
	cell.update();
	cell.draw();

	for (var i = food.length-1; i >= 0; i--) {
		food[i].draw();
		if (cell.eats(food[i])) {
			food.splice(i, 1);
			//addNewFood(20);
		}
	}
	console.clear();
	console.log(food.length);
}

function keyPressed() {
	cell.keyPressed();
}

function keyReleased() {
}

/*function keyReleased() {
	if (keyCode == UP_ARROW) {
	} else if (keyCode == DOWN_ARROW) {
	} else if (keyCode == LEFT_ARROW) {
		tetris.movingLeft = false;
		tetris.lastMoveSideways = millis();
	} else if (keyCode == RIGHT_ARROW) {
		tetris.movingRight = false;
	}
}*/

function mousePressed() {
}

function mouseMoved() {
	cell.mouseMoved();
}
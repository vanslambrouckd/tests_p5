var objs = [];

function setup() {
  createCanvas(600, 300);
  for (var i = 0; i < 15; i++) {
    var obj = new Obj(random(0, width-30), random(0, (height-30)));
    objs.push(obj);
  }
}

function draw() {
  background(0);

  for(var i = objs.length-1; i>= 0;i--){
    objs[i].update();
    objs[i].draw();
  }
}

function mousePressed() {
  for(var i = objs.length-1; i>= 0;i--){
    objs[i].mousePressed();
  }
}

function mouseReleased() {
  for(var i = objs.length-1; i>= 0;i--){
    objs[i].mouseReleased();
  }
}

function Obj(x, y) {
  this.itemtype = 'RECT';
  this.loc = createVector(x, y);
  this.w = 30;
  this.h = 30;
  this.hover = false;
  this.dragging = false;
  this.clicked_offset_x = 0;
  this.clicked_offset_y = 0;

  this.mouseReleased = function() {
    this.hover = false;
    this.dragging = false;
  }

  this.mousePressed = function() {
      if (this.hover) {
          this.clicked_offset_x = mouseX - this.loc.x;
          this.clicked_offset_y = mouseY - this.loc.y;
          this.dragging = true;
      } else {
        this.dragging = false;
      }
  }

  this.draw = function() {
    this.hover = this.selected();

    if (this.dragging) {
      this.loc.x = mouseX-this.clicked_offset_x;
      this.loc.y = mouseY-this.clicked_offset_y;
    }

    rect(this.loc.x, this.loc.y, this.w, this.h);
  }

  this.update = function() {
  }

  // this.intersects = function(other) {
  //   if (other.itemtype == 'RECT') {
  //       var x1 = createVector(this.loc.x, this.loc.y);
  //       var x2 = createVector(this.loc.x+this.w, this.loc.y);
  //       var x3 = createVector(this.loc.x, this.loc.y+this.h);
  //       var x4 = createVector(this.loc.x+this.w, this.loc.y+this.h);
  //
  //       if (x1.x > other.loc.x && (x1.x <= (other.loc.x+other.w))) {
  //         //intersects horizontally
  //       }
  //
  //
  //       print("jaa");
  //     return true;
  //   }
  //   return false;
  // }

  this.selected = function() {
    if (mouseX >= this.loc.x && (mouseX <= this.loc.x+this.w)
    && (mouseY >= this.loc.y) && (mouseY <= this.loc.y+this.h)) {
      return true;
    }
    return false;
  }
}


var circle;
var rectangle;
var intersects = false;

function setup() {	
	createCanvas(400, 400);
    rectangle = new Rectangle(140, 140, 50, 50);
    circle = new Circle(200, 200, 20);
}

function draw() {
	background(0);
	circle.draw();
	rectangle.draw();

	intersects = intersect(circle, rectangle)
}

function Circle(x, y, r) {
	this.x = x;
	this.y = y;
	this.radius = r;

	this.draw = function() {
		fill(255);
		ellipse(this.x, this.y, this.radius*2, this.radius*2);
	}
}

function Rectangle(x, y, width, height) {
	this.x = x;
	this.y = y;
	this.w = width;
	this.h = height;

	this.draw = function() {
		fill(100);
		if (intersects) {
			fill(255,0,0);
		}
		rect(this.x, this.y, this.w, this.h);
	}
}

function intersect(circle, rect) {
	//https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection	
	var topleft = rect.x;
	var topright = rect.x + rect.w;
	var bottomleft = rect.x + rect.h;
	var bottomright = rect.y + rect.h;

	var closestX, closestY;

	if (circle.x < rect.x) {
		closestX = rect.x;
	} else if (circle.x > topright) {
		closestX = topright;
	} else {
		closestX = circle.x;
	}

	//http://www.migapro.com/circle-and-rotated-rectangle-collision-detection/
	if (circle.y < rect.y) {
		closestY = rect.y;
	} else if (circle.y > bottomright) {
		closestY = bottomright;
	} else {
		closestY = circle.y;
	}

	var collision = false;
	var d = findDistance(circle.x, circle.y, closestX, closestY);
	var radius = circle.radius;
	// console.log('closestX', closestX);
	// console.log('closestY', closestY);
	// console.log('d', d);
	// console.log(radius);
	if (d < radius) {
		collision = true;
	} else {
		collision = false;
	}

	return collision;
}

function findDistance(fromX, fromY, toX, toY){
    var a = Math.abs(fromX - toX);
    var b = Math.abs(fromY - toY);
 
    return Math.sqrt((a * a) + (b * b));
}

function keyPressed() {
	if (keyCode == UP_ARROW) {
		rectangle.y -= 1;
	} else if (keyCode == DOWN_ARROW) {
		rectangle.y += 1;
	} else if (keyCode == LEFT_ARROW) {
		rectangle.x -= 1;
	} else if (keyCode == RIGHT_ARROW) {
		rectangle.x += 1;
	}
}
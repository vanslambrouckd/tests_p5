var snake;
var food;
var pKeyCode;

var codes = [];
var rows = 4;
var cols = 5;
var cellWidth;
var cellHeight;

var reveal = [];
var revealed = [];

var controlsPaused = [];

function removePause(name) {
	if (controlsPaused[name] != null) {
		controlsPaused[name] = null;
	}
}

function addPause(name, ms) {
	if (controlsPaused[name] == null) {
		//controlsPaused[name] = millis();
		controlsPaused[name] = new Timer(ms);
		controlsPaused[name].start();
		console.log(name + " paused");
	}
}

function checkPauses() {
	console.clear();
	console.log(controlsPaused);
	for (var name in controlsPaused) {
		if (controlsPaused[name] != null) {
			var timer = controlsPaused[name];
			console.log(timer.startTime);
			if (timer.ended()) {
				controlsPaused[name] = null;
				console.log(name + " resumed");
			}		
		}
	}
}


function isPaused(name) {
	if (controlsPaused[name] != null) {
		var timer = controlsPaused[name];
		if (timer.ended()) {
			controlsPaused[name] = null;
			return false;
		}		

		return true;
	}

	return false;
}

function setup() {
	createCanvas(640, 400);
	frameRate(10);
	
	for (var i = 0; i < 10; i++) {
		codes[i] = i;
	}

	for (var i = 0; i < 10; i++) {
		codes[i+10] = i;
	}

	cellWidth = 640/cols;
	cellHeight = 400/rows;

	//codes = shuffle(codes);
	// console.log("codes");
	// console.log(codes);
	// console.log("imgs");
	// console.log(imgs);
	
}

function drawGrid() {
	var teller = 0;
	for (var col = 0; col < cols; col++) {
		for (var row = 0; row < rows; row++) {
			//console.log(row, col);
			var x = col*cellWidth;
			var y = row*cellHeight;
			//fill(random(255), random(255), random(255));
			rect(x, y, cellWidth-2, cellHeight-2);
			fill(255);
			textSize(15);
			text("row: "+row, x, y+20);
			text("col: "+col, x, y+35);
			text("index: "+getColIndex(row, col), x, y+50);
			teller++;

		}
	}
}

function getColIndex(row, col) {
	return (row)*cols+col;
}

function getRow(index) {

}

var imgs = [];

function preload() {
	for (var i = 0; i < 10; i++) {
		imgs[i] = loadImage("img/"+(i+1)+".png");
	}
}

function draw() {
	checkPauses();

	background("#0000");
	drawGrid();

	//console.clear();
	//console.log(revealed);

	drawCurrentSelection();
	drawRevealed();

	if (!isPaused("choose_img1")) {

	} else {
		//animatie tonen
	}
}

function drawRevealed() {
	//console.clear();
	for (var i = 0; i < revealed.length; i++) {
		var index = revealed[i];
		//console.log(index);
		showImage(index);
	}
}

function drawCurrentSelection() {
	for (var i = 0; i < reveal.length; i++) {
		showImage(reveal[i]);
	}
}

function keyPressed() {
	// if (keyCode == UP_ARROW) {
	// 	snake.dir(0, -1);
	// } else if (keyCode == DOWN_ARROW) {
	// 	snake.dir(0, 1);
	// } else if (keyCode == LEFT_ARROW) {
	// 	snake.dir(-1, 0);		
	// } else if (keyCode == RIGHT_ARROW) {
	// 	snake.dir(1, 0);
	// }
	// pKeyCode = keyCode;
}

function mousePressed() {
	var col = floor(mouseX/cellWidth);
	var row = floor(mouseY/cellHeight);
	if (!isPaused('choose_img1')) {
		var index = getColIndex(row, col);
		//console.log("col:" + col, "row: "+row);
		//console.log('index: ', index);

		if (index < (rows*cols)) {
			showImage(index);		

			if (reveal.length > 1) {
				reveal = [];
			}

			if (reveal.length == 1) {
				addPause("choose_img1", 2000);
				//console.log('add pause');				
			}

			reveal.push(index);
			if (reveal.length == 2) {
				if (codes[reveal[0]] == codes[reveal[1]]) {
					revealed.push(reveal[0]);
					revealed.push(reveal[1]);
					removePause("choose_img1");
				}
			}
		}
	}
}

function getCol(index) {
	return index%cols;
}

function getRow(index) {	
	return floor(index/cols);
}

function showImage(index) {
	//console.log('showImage');
	//console.clear();

	var code = codes[index];
	// console.log("index: "+index);
	// console.log("code: "+code);
	// console.log(imgs[code]);
	var row = getRow(index);
	var col = getCol(index);
	// console.log('index', index);
	// console.log('row', row);
	// console.log('col', col);
	image(imgs[code], col*cellWidth, row*cellHeight, cellWidth, cellHeight);		
}
function Snake() {
	this.loc = this.pLoc = createVector(0, 0);
	this.speed = createVector(this.cellSize, 0);

	this.cellSize = 20;
	this.totalLength = 0;
	this.tail = [];

	this.update = function() {
		for (var i = 0; i < this.tail.length-1; i++) {
			this.tail[i] = this.tail[i+1];
		}

		if (this.totalLength > 0) {
			this.tail[this.totalLength-1] = createVector(this.loc.x, this.loc.y); //laaste item in de array is de head
		}

		this.pLoc = createVector(this.loc.x, this.loc.y); //previous location
		this.loc.add(this.speed);


		if (this.loc.y < 0) {
			this.loc.y = height;
		}
		if (this.loc.y > height) {
			this.loc.y = 0;
		}

		if (this.loc.x < 0) {
			this.loc.x = width;
		}
		if (this.loc.x > width) {
			this.loc.x = 0;
		}
		//this.x = constrain(this.x, 0, width-this.cellSize);
		//this.y = constrain(this.y, 0, height-this.cellSize);
	}

	this.dir = function(x, y) {
		this.speed = createVector(x*this.cellSize, y*this.cellSize);
	}

	this.eat = function(food) {
		var d = dist(this.loc.x, this.loc.y, food.loc.x, food.loc.y);
		if (d < 1) {
			this.totalLength++;
			return true;
		}

		return false;
	}

	this.show = function() {
		//tail tonen
		for (var i = 0; i < this.tail.length; i++) {
			rect(this.tail[i].x, this.tail[i].y, this.cellSize, this.cellSize);
		}


		//head tonen
		rect(this.loc.x, this.loc.y, this.cellSize, this.cellSize);
	}

	this.death = function() {
		for (var i = 0; i < this.tail.length; i++) {
			var pos = this.tail[i];
			var d = dist(this.loc.x, this.loc.y, pos.x, pos.y);
			if (d < 1) {
				this.totalLength = 0;
				this.tail = [];
				console.log('death');
			}
		}
	}
}
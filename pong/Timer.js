function Timer(duration) {
	this.startTime = null;
	this.duration = duration;

	this.start = function() {
		this.startTime = millis();
	}

	this.ended = function() {
		var timeLeft = this.timeLeft();
		if (timeLeft >= this.duration) {
			return true;
		}

		return false;
	}

	this.timeLeft = function() {
		return millis()-this.startTime;		
	}
}
function Paddle(pong, x, keymap) {
	this.keymap = keymap;
	this.width = 20;
	this.x = x;
	this.ychange = 0;
	if (this.x > 0) {
		this.x = this.x-this.width;
	}
	this.height = 100;
	this.y = (height/2)-(this.height/2);
	//console.log(this.x+this.width);

	this.update = function() {
		this.y += this.ychange;
		this.y = constrain(this.y, 0, height-this.height);
	}

	this.move = function(ychange) {
		this.ychange = ychange;
	}

	this.draw = function() {
		rect(this.x, this.y, this.width, this.height);
	}

	this.keyPressed = function() {
		//console.log(keyCode);
		if (keyCode == this.keymap['UP']) {
			this.move(-5);
		} else if (keyCode == this.keymap['DOWN']) {
			this.move(5);
		}
	}

	this.keyReleased = function() {
		this.move(0);
	}

	this.isP1 = function() {
		//p1 = left paddle
		//p2 = right paddle
		if (this.x == 0) {
			return true;
		}
		return false;
	}

	this.getCollisionAngle = function(puck) {
		var collisionPointY = puck.loc.y - (this.y);

		//http://zekechan.net/wp-content/uploads/2015/07/pong-05b.png
		if (this.isP1()) {	
			//console.log('collisionPointY', collisionPointY);
			var deg = radians(45);
			var angle = map(collisionPointY, 0, this.height, -deg, deg);
		} else {
			var angle = map(collisionPointY, 0, this.height, radians(225), radians(135));
		}
		return angle;
	}
}
function Pong(soundeffects) {
    // this.paddle_left = new Paddle(this, 0, {'UP': UP_ARROW, 'DOWN': DOWN_ARROW});
    // this.paddle_right = new Paddle(this, width, {'UP': 65, 'DOWN': 90});
    // this.puck = new Puck(this, width/2, height/2);

    this.gameover = false;
    this.soundeffects = soundeffects;
    this.scoreP1 = 9;
    this.scoreP2 = 0;
    this.controlsPaused = null;

    this.resetControls = function() {
        this.paddle_left = new Paddle(this, 0, {'UP': UP_ARROW, 'DOWN': DOWN_ARROW});
        this.paddle_right = new Paddle(this, width, {'UP': 65, 'DOWN': 90});
        this.puck = new Puck(this, 33, height/2);
    }

    this.resetControls();

    this.pauseControls = function() {
        if (this.controlsPaused == null) {
            console.log('controlsPaused gezt');
            this.controlsPaused = millis();
        }
    }

    this.resumeControls = function() {
        this.controlsPaused = null;
    }

    this.playIsOver = function(scoreP1Addition, scoreP2Addition) {
        if (this.controlsPaused == null) {
            this.controlsPaused = millis();
            this.scoreP1 += scoreP1Addition;
            this.scoreP2 += scoreP2Addition;
        } else {
            if (millis() - this.controlsPaused >= 500) {
                this.controlsPaused = null;
                this.resetControls();
            }
        }
    }
    
    this.isGameOver = function() {
        var gameover = this.scoreP1 >= 10;
        var gameover2 = this.scoreP2 >= 10;
        if (gameover || gameover2) {
            this.gameover = true;
            this.soundeffects['gameOver'].play();
            background(255);
            fill(0);
            textSize(36);
            textAlign(CENTER);
            var startY = width/2;
            text('GAME OVER', width/2, startY);

            textSize(20);

            startY += 30;
            if (this.scoreP1 > this.scoreP2) {
                text('PLAYER 1 wins', width/2, startY);
            } else {
                text('PLAYER 2 wins', width/2, startY);
            }
            startY+=30;
            text(this.scoreP1+' - '+this.scoreP2, width/2, startY);

            textSize(14);
            startY += 22;
            text('press space to restart', width/2, startY);
            noLoop();
        }
    }

    this.draw = function() {
        //console.log(millis());
        background(0);
        stroke(255);
        line(width/2, 0, width/2, height);
        
        this.paddle_left.draw();
        this.paddle_right.draw();

        this.paddle_left.update();
        this.puck.draw();

        //this.playIsOver();
        this.isGameOver();

        this.drawScore();
    }

    this.drawScore = function() {
        fill(255);
        textAlign(CENTER);
        textSize(40);
        text(this.scoreP1, width/4, 50);
        text(this.scoreP2, (width/4)*3, 50);
    }

    this.update = function() {
        this.paddle_right.update();
        this.puck.checkPaddle(this.paddle_left);
        this.puck.checkPaddle(this.paddle_right);
        this.puck.update();            
    }

    this.keyPressed = function() {
        if (this.controlsPaused == null) {
            this.paddle_left.keyPressed();
            this.paddle_right.keyPressed();
            console.log('jaa');
            if (this.gameover) {
                console.log('gameeee overeee');
                if (keyCode == 32) { //spatie
                    this.resetControls();
                    this.gameover = false;
                    this.scoreP1 = 0;
                    this.scoreP2 = 0;
                    loop();
                }
            }
        }
    }

    this.keyReleased = function() {
        this.paddle_left.keyReleased();
        this.paddle_right.keyReleased();
    }
}
function Puck(pong, x, y) {
	this.r = 12;
	this.loc = createVector(x, y);
	this.velocity = createVector(-5,0);

	this.update = function() {
		this.loc.add(this.velocity);

		// var circle = {
		// 	x: this.loc.x, 
		// 	y: this.loc.y, 
		// 	radius: this.r
		// }

		if (this.velocity.x < 0) {
			var check_paddle = pong.paddle_left;
		} else {
			var check_paddle = pong.paddle_right;
		}

		// var rectangle = {
		// 	x: check_paddle.x,
		// 	y: check_paddle.y,
		// 	w: check_paddle.width,
		// 	h: check_paddle.height,
		// }
					
		if (!this.isBetweenPaddleHeight(check_paddle)) {
			if ((this.loc.x-this.r) < pong.paddle_left.width) {			
				pong.playIsOver(0, 1);	
			} else if ((this.loc.x+this.r) > width-(pong.paddle_right.width-5)) {
				pong.playIsOver(1, 0);
			}	
		}

		if (this.loc.y > height) {
			this.velocity.y *= -1;
		}

		if (this.loc.y < 0) {
			this.velocity.y *= -1;
		}
	}

	this.draw = function() {
		fill(255);
		ellipse(this.loc.x, this.loc.y, this.r*2, this.r*2);
	}

	this.isBetweenPaddleHeight = function(paddle) {
		// console.log('puck y', this.loc.y);
		// console.log('paddle ytop', paddle.y);
		// console.log('paddle ybottom', (paddle.y+paddle.height));
		if ( ((this.loc.y+this.r) >= paddle.y) && ((this.loc.y-this.r) <= (paddle.y+paddle.height) ) ) {
			return true;
		}
	}

	this.isCollision = function(paddle) {
		if (!paddle.isP1()) {	
			if (this.loc.x+this.r > paddle.x) {
				return true;
			}
		} else {
			if (this.loc.x-this.r < paddle.x+paddle.width) {
				return true;
			}
		}

		return false;
	}

	this.checkPaddle = function(paddle) {
		//if (paddle.x > 0) {
		if (this.isBetweenPaddleHeight(paddle)) {
			if (this.isCollision(paddle)) {
				if (paddle.isP1()) {	
					pong.soundeffects['collisionLeft'].play();
				} else {
					pong.soundeffects['collisionRight'].play();
				}

				var angle = paddle.getCollisionAngle(this);

				this.velocity.x = -this.velocity.x;
				this.velocity.y = 5*sin(angle);
			}
		}
	}
}
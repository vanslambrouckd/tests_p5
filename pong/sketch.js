
var pong;
var soundeffects = [];
var circle;
var rectangle;
var intersects = false;

function setup() {	
	createCanvas(400, 400);
	// frameRate(1);
    pong = new Pong(soundeffects);
}

function preload() {
	soundeffects['collisionLeft'] = loadSound('sounds/collision_left.wav');
    soundeffects['collisionRight'] = loadSound('sounds/collision_right.wav');
    soundeffects['gameOver'] = loadSound('sounds/game_over.wav');
}

function draw() {
	pong.update();
	pong.draw();
	//noLoop();
}

function keyPressed() {
	pong.keyPressed();
}

function keyReleased() {
	pong.keyReleased();
}


function findDistance(fromX, fromY, toX, toY){
    var a = Math.abs(fromX - toX);
    var b = Math.abs(fromY - toY);
 
    return Math.sqrt((a * a) + (b * b));
}

function findDistanceDiff(circle, rect) {
	//https://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection	
	var topleft = rect.x;
	var topright = rect.x + rect.w;
	var bottomleft = rect.x + rect.h;
	var bottomright = rect.y + rect.h;

	var closestX, closestY;

	if (circle.x < rect.x) {
		closestX = rect.x;
	} else if (circle.x > topright) {
		closestX = topright;
	} else {
		closestX = circle.x;
	}

	//http://www.migapro.com/circle-and-rotated-rectangle-collision-detection/
	if (circle.y < rect.y) {
		closestY = rect.y;
	} else if (circle.y > bottomright) {
		closestY = bottomright;
	} else {
		closestY = circle.y;
	}

	var d = findDistance(circle.x, circle.y, closestX, closestY);
	return d;
}

function intersect(circle, rect) {
	var radius = circle.radius;
	var d = findDistanceDiff(circle, rect);
	// console.log('closestX', closestX);
	// console.log('closestY', closestY);
	
	// console.log(radius);
	var collision = false;

	if (d < radius) {
		collision = true;
	} else {
		collision = false;
	}

	return collision;
}
function Food(x, y) {
	this.cellSize = 20;

	this.changeLocation = function() {
		var rows = floor(height/this.cellSize);
		var cols = floor(width/this.cellSize);

		this.loc = createVector(floor(random(cols)), floor(random(rows)));
		this.loc.mult(this.cellSize);
	}

	this.changeLocation();
	this.show = function() {
		fill(255);
		rect(this.loc.x, this.loc.y, this.cellSize, this.cellSize);
	}
}
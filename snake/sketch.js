var snake;
var food;
var pKeyCode;

function setup() {
	createCanvas(640, 400);
	frameRate(10);
	snake = new Snake();
	//food = new Food(random(width), random(height));
	food = new Food(10,10);
}

function draw() {
	background(0);

	if (snake.eat(food)) {
		food.changeLocation();
	}
	food.show();

	snake.death();
	snake.update();
	snake.show();

}

function keyPressed() {
	if (keyCode == UP_ARROW) {
		snake.dir(0, -1);
	} else if (keyCode == DOWN_ARROW) {
		snake.dir(0, 1);
	} else if (keyCode == LEFT_ARROW) {
		snake.dir(-1, 0);		
	} else if (keyCode == RIGHT_ARROW) {
		snake.dir(1, 0);
	}
	pKeyCode = keyCode;
}

function mousePressed() {
	snake.totalLength++;
}
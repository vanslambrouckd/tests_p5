var tetris;
var fallingPiece = null, nextPiece = null;
var lastFallTime;
var lastMoveDown;
var lastMoveSideways;

function setup() {	
	createCanvas(400, 400);
	frameRate(60);

	tetris = new Tetris();
}

function draw() {
	background(0);
	tetris.update();
	tetris.draw();
	//console.log(tetris.fallingPiece.letter);
}

function keyPressed() {
	/*if (keyCode == UP_ARROW) {
		tetris.rotatePiece(tetris.fallingPiece);
	} else if (keyCode == DOWN_ARROW) {
		if (tetris.isValidPosition(tetris.fallingPiece)) {
			tetris.fallingPiece.y += 1;
		}
	} else if (keyCode == LEFT_ARROW) {
		tetris.lastMoveSideways = millis();
	} else if (keyCode == RIGHT_ARROW) {
	}*/
	tetris.keyPressed();
	pKeyCode = keyCode;
}

function keyReleased() {
	tetris.keyReleased();
}

/*function keyReleased() {
	if (keyCode == UP_ARROW) {
	} else if (keyCode == DOWN_ARROW) {
	} else if (keyCode == LEFT_ARROW) {
		tetris.movingLeft = false;
		tetris.lastMoveSideways = millis();
	} else if (keyCode == RIGHT_ARROW) {
		tetris.movingRight = false;
	}
}*/

function mousePressed() {
}
function Tetris() {
	this.fallFreq = 1000;
	this.templateWidth = 3;
	this.templateHeight = 3;
	this.boxSize = 20;
	this.boardwidth = 10;
	this.boardheight = 20;
	this.xMargin = width-(this.boardwidth*this.boxSize)/2;
	this.topMargin = height-(this.boardHeight*this.boxSize)-5;

	this.movingLeft = false;
	this.movingRight = false;
	this.movingDown = false;

	this.lastFallTime = 0;
	this.lastMoveDownTime = millis();
	this.lastMoveSidewaysTime = millis();
	this.moveDownFreq = 200;
	this.sidewaysFreq = 100;
	
	this.getBlankBoard = function() {
		var board = [];
		for (var y = 0; y < this.boardheight; y++) {
			board[y] = [];
			for (var x = 0; x < this.boardwidth; x++) {
				board[y][x] = '';
			}
		}

		for (var x = 0; x < this.boardwidth; x++){
			board[this.boardheight-1][x] = 1;
		}

/*		board[14][6] = 1;
		board[14][7] = 1;
		board[13][6] = 1;
		board[13][7] = 1;*/

		var col = color(255,0,0);
		board[18][0] = col;
		board[18][1] = col;
		board[18][2] = col;
		board[18][3] = col;
		board[18][4] = col;
		board[18][5] = col;
		board[18][6] = col;
		board[18][7] = col;

		board[17][0] = col;
		board[17][1] = col;
		board[17][2] = col;
		board[17][3] = col;
		board[17][4] = col;
		board[17][5] = col;
		board[17][6] = col;


		return board;
	}

	this.board = this.getBlankBoard();

	this.isValidPosition = function(piece, adjX, adjY) {
		var shape = piece.shape[piece.rotation];
		/*console.log(adjX);		
		console.log(adjY);		*/
		for (var j = 0; j < this.templateHeight; j++) {
			for (var i = 0; i < this.templateWidth; i++) {
				var block = shape[j][i];
				if (block == '1') {
					var xPos = i+piece.x+adjX;
					var yPos = j+piece.y+adjY;
					//console.log(xPos, yPos, j);
					if (yPos > this.boardheight-1) {
						return false;
					}
					if (xPos < 0) {
						return false;
					}

					if (xPos >= this.boardwidth) {
						return false;
					}

					if (this.board[yPos][xPos] != '') {
						console.log(xPos, yPos, ' col, row, invalid positionee');
						return false;
					} else {
					}
				}
			}
		}
		return this.isAboveBoard(piece);
	}

	this.isAboveBoard = function(piece) {
		var shape = piece.shape[piece.rotation];
		for (var j = 0; j < this.templateHeight; j++) {
			for (var i = 0; i < this.templateWidth; i++) {
				var block = shape[j][i];
				if (block == '1') {
					//console.log(piece.y, j);
					if (j+piece.y+2 > this.boardheight) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/*function pieceToBoardCoords(piece) {
		var x = piece.x;
		var y = piece.y;
		y += piece.
	}*/

	this.addToBoard = function(piece) {
		var shape = piece.shape[piece.rotation];
		for (var j = 0; j < this.templateHeight; j++) {
			for (var i = 0; i < this.templateWidth; i++) {
				if (shape[j][i] == 1) {
					this.board[j+piece.y][i+piece.x] = piece.color;
				}
			}
		}
	}
	
	this.createPiece = function(type) {
		switch(type) {
			case 'I':
				var rot_a = [
					[0,1,0],
					[0,1,0],
					[0,1,0]
				];

				var rot_b = [
					[0,0,0],
					[1,1,1],
					[0,0,0]
				];

				return [
					rot_a,
					rot_b,
					rot_a,
					rot_b,
				];
			break;

			case 'T':
				var rot_a = [
					[1,1,1],
					[0,1,0],
					[0,0,0]
				];

				var rot_b = 
				[
					[0,0,1],
					[0,1,1],
					[0,0,1]
				];

				var rot_c = 
				[
					[0,1,0],
					[1,1,1],
					[0,0,0]
				];

				var rot_d = 
				[
					[0,1,0],
					[0,1,1],
					[0,1,0]
				];

				return [
					rot_a,
					rot_b,
					rot_c,
					rot_d
				];
			break;

			case 'J':
				var rot_a = [
					[1,1,1],
					[0,0,1],
					[0,0,0]
				];

				var rot_b = 
				[
					[0,0,1],
					[0,0,1],
					[0,1,1]
				];

				var rot_c = 
				[
					[1,0,0],
					[1,1,1],
					[0,0,0]
				];

				var rot_d = 
				[
					[0,1,1],
					[0,1,0],
					[0,1,0]
				];

				return [
					rot_a,
					rot_b,
					rot_c,
					rot_d
				];
			break;

			case 'L':
				var rot_a = [
					[1,1,1],
					[1,0,0],
					[0,0,0]
				];

				var rot_b = 
				[
					[1,1,0],
					[0,1,0],
					[0,1,0]
				];

				var rot_c = 
				[
					[0,0,1],
					[1,1,1],
					[0,0,0]
				];

				var rot_d = 
				[
					[0,1,0],
					[0,1,0],
					[0,1,1]
				];

				return [
					rot_a,
					rot_b,
					rot_c,
					rot_d
				];
			break;

			case 'S':
				var rot_a = [
					[0,1,1],
					[1,1,0],
					[0,0,0]
				];

				var rot_b = 
				[
					[1,0,0],
					[1,1,0],
					[0,1,0]
				];

				return [
					rot_a,
					rot_b,
					rot_a,
					rot_b,
				];
			break;

			case 'Z':
				var rot_a = [
					[1,1,0],
					[0,1,1],
					[0,0,0]
				];

				var rot_b = 
				[
					[0,0,1],
					[0,1,1],
					[0,1,0]
				];

				return [
					rot_a,
					rot_b,
					rot_a,
					rot_b
				];
			break;

			case 'O':
				var rot_a = [
					[1,1,0],
					[1,1,0],
					[0,0,0]
				];

				return [
					rot_a,
					rot_a,
					rot_a,
					rot_a,
				];
			break;
		}
	}

	this.update = function() {
		/*var diff = millis()-this.lastFallTime;
		if (diff >= this.fallFreq) {
			this.lastFallTime = millis();
			if (this.isValidPosition(this.fallingPiece, 0, 1)) {
				this.fallingPiece.y += 1;
			} 
			//this.movingLeft = this.movingRight = false;
		}*/

		var diffDown= millis()-this.lastMoveDownTime;
		if (diffDown > 10) {
			this.lastMoveDownTime = millis();
			if (this.movingDown) {
				if (this.isValidPosition(this.fallingPiece, 0, 1)) {
					this.fallingPiece.y += 1;
				}	
			}
			//this.moveDown = false;
		}

		var diffSideWays = millis()-this.lastMoveSidewaysTime;
		/*console.clear();
		console.log(diffSideWays);	*/
		if (diffSideWays > this.sidewaysFreq) {
			this.lastMoveSidewaysTime = millis();
			if (this.movingLeft) {
				if (this.isValidPosition(this.fallingPiece, -1, 0)) {
					//console.log(this.fallingPiece.x)
					this.fallingPiece.x -= 1;					
				}
				//console.log('moving lefttt');
				//this.movingLeft = this.movingRight = false;
			}

			if (this.movingRight) {
				//console.log('moving rightt');
				if (this.isValidPosition(this.fallingPiece, 1, 0)) {
					this.fallingPiece.x += 1;
				}
				//this.movingLeft = this.movingRight = false;
			}

			//this.fallingPiece.x = constrain(this.fallingPiece.x, 0, this.boardwidth-this.getPieceWidth(this.fallingPiece));
			/*this.fallingPiece.x = constrain(this.fallingPiece.x, 0, 8);
			console.log(this.fallingPiece.x);
			//console.log(this.boardwidth);
			console.log('test='+(this.boardwidth-this.getPieceWidth(this.fallingPiece)));*/
		}

		
		if (!this.isValidPosition(this.fallingPiece, 0, 1)) {
			this.addToBoard(this.fallingPiece);
			//this.fallingPiece = this.getNewPiece();
			this.fallingPiece = this.nextPiece;
			this.nextPiece = this.getNewPiece();
		}
		this.removeCompleteLines();
	}

	this.removeCompleteLines = function() {
		for (var y = 0; y < this.boardheight; y++) {
			if (this.isCompleteLine(y)) {
				this.lowerGrid(y);
			}
		}
	}

	this.lowerGrid =function(startY) {
		//for (var y = this.boardheight-2; y > 0;  y--) {
		for (var y = startY-1; y > 0;  y--) {
			for (var x = 0; x < this.boardwidth; x++) {
				//console.log(this.board[y]);
				this.board[y+1][x] = this.board[y][x];
			}
		}
	}

	this.isCompleteLine = function(y) {
		var total = 0;
		for (var x = 0; x < this.boardwidth; x++) {
			if (this.board[y][x] != '') {
				total++;
			}
		}
		//console.log(total);
		return total == this.boardwidth;
	}

	this.isCompleteLine(19);

	this.getPieceWidth = function(piece) {
		var shape = piece.shape[piece.rotation];

		//console.log(shape);
		var shapeWidth = 0;
		for (var i = 0; i < this.templateWidth; i++) {
			for (var j = 0; j < this.templateHeight; j++) {
				var block = shape[i][j];
				if (block == '1') {
					//console.log(j);
					if (j > shapeWidth) {
						shapeWidth = j;
					}
				}
			}
		}		
		shapeWidth += 1;
//		console.log('shapeWidth='+shapeWidth);
		return shapeWidth;
	}

	this.getNewPiece = function() {
		var shapes = ['S', 'Z', 'T', 'I', 'O', 'J', 'L'];

		var type = Math.floor(Math.random()*shapes.length);
		type = shapes[type];
		//type = 'T';

		var shape = {
			'color': color(random(255), random(255), 0),
			'shape': this.createPiece(type),
			'letter': type,
			'x': this.boardwidth/2,
			'y': 0,
			'rotation': 0,
		};


		shape.x = Math.floor(random(0, 7));
		//console.log('shape.x', shape.x);
		return shape;
	}

	this.fallingPiece = this.getNewPiece();
	this.nextPiece = this.getNewPiece();

	this.draw = function() {
		this.drawBoard();
		if (this.fallingPiece !== undefined) {
			this.drawPiece(this.fallingPiece);
		}
	}

	this.drawBoard = function() {
		strokeWeight(1);
		stroke(100);
		//draw grid
		for (var x = 0; x < this.boardwidth; x++) {
			for (var y = 0; y < this.boardheight; y++) {
				rect(x*this.boxSize, y*this.boxSize, this.boxSize, this.boxSize);
				if (this.board[y][x] != '') {
					var col = color(red(this.board[y][x]), green(this.board[y][x]), blue(this.board[y][x]));
					this.drawBox(x, y, col);
				} else {
					this.drawBox(x, y, color(255,255,255));
				}
			}
		}		

		//draw next element
		for (var x = 0; x < this.templateWidth; x++) {
			for (var y = 0; y < this.templateHeight; y++) {
				var shape = this.nextPiece.shape[0];
				if (shape[y][x] == 1) {
					var col = color(red(this.nextPiece.color), green(this.nextPiece.color), blue(this.nextPiece.color));
					push();
					translate(220, 50);
					this.drawBox(x, y, col);
					translate(0, 0);
					pop();
				}
			}
		}
	}

	this.drawBox = function(x, y, col) {
		fill(red(col), green(col), blue(col));
		rect(x*this.boxSize, y*this.boxSize, this.boxSize, this.boxSize);
		fill(255)
	}

	this.drawPiece = function(piece) {
		var shape = piece.shape[piece.rotation];
		for (var i = 0; i < this.templateWidth; i++) {
			for (var j = 0; j < this.templateHeight; j++) {
				if (shape[i][j] == '1') {
					var yoffset = piece.y;
					var xoffset = piece.x;
					this.drawBox(j+xoffset, i+yoffset, piece.color);
				}
			}
		}
	}

	this.rotatePiece = function(piece) {
		shape = piece.shape;

		var rot = piece.rotation;
		rot+=1;
		console.log(rot);
		piece.rotation = rot%4;

		/*if (piece.rotation == 0) {
			piece.rotation = 1;
		} else {
			piece.rotation = 0;
		}*/
	}

	this.keyPressed = function() {
		if (keyCode == 82) { //letter R
			this.removeCompleteLines();
		}
		if (keyCode == UP_ARROW) {
			this.rotatePiece(this.fallingPiece);
			if (!this.isValidPosition(this.fallingPiece, 0,0)) {
				this.rotatePiece(this.fallingPiece);
			}
		} else if (keyCode == DOWN_ARROW) {
			this.movingDown = true;
		} else if (keyCode == LEFT_ARROW) {
			this.movingLeft = true;
			this.movingRight = false;
			this.lastMoveSideways = millis();
		} else if (keyCode == RIGHT_ARROW) {
			this.movingRight = true;
			this.movingLeft = false;
			this.lastMoveSideways = millis();
		}
		pKeyCode = keyCode;
	}

	this.keyReleased = function() {	
		if (keyCode == DOWN_ARROW) {
			this.movingDown = false;
		} else if (keyCode == LEFT_ARROW) {
			this.movingLeft = false;
			this.movingRight = false;
		} else if (keyCode == RIGHT_ARROW) {
			this.movingLeft = false;
			this.movingRight = false;
		}
	}

	
}